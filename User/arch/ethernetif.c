/**
  ******************************************************************************
  * File Name          : ethernetif.c
  * Description        : This file provides code for the configuration
  *                      of the ethernetif.c MiddleWare.
  ******************************************************************************
  * This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * Copyright (c) 2018 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/

#include "ethernetif.h"
#include <string.h>
#include "usb_rndis_core.h"

static uint8_t received[RNDIS_MTU + 14];
static int recvSize;
static sys_sem_t rx_sem = NULL;

struct ethernetif {
	struct eth_addr *ethaddr;
/* Add whatever per-interface state that is needed here. */
};

err_t linkoutput_fn(struct netif *netif, struct pbuf *p)
{
    struct pbuf *q;
    static char data[RNDIS_MTU + 14 + 4];
    int size = 0;
		
    for(q = p; q != NULL; q = q->next)
    {
        if (size + q->len > RNDIS_MTU + 14)
            return ERR_ARG;
        memcpy(data + size, (char *)q->payload, q->len);
        size += q->len;
    }
		LED2_TOGGLE;
    if (!rndis_can_send())
        return ERR_USE;
    rndis_send(data, size);
//    outputs++;
    return ERR_OK;
}

void ethernetif_input(void *pParams) {
	struct netif *netif;
	struct pbuf *frame;
	netif = (struct netif*) pParams;
  LWIP_DEBUGF(NETIF_DEBUG, ("ethernetif_input: IP input error\n"));
  
	while(1) 
  {
      sys_arch_sem_wait( rx_sem, 0 );
      /* move received packet into a new pbuf */
      taskENTER_CRITICAL();
			if (recvSize == 0) { continue;	}
			frame = pbuf_alloc(PBUF_RAW, recvSize, PBUF_POOL);
			if (frame == NULL) {	continue;	}
			memcpy(frame->payload, received, recvSize);
			frame->len = recvSize;
			recvSize = 0;
			if (netif->input(frame, netif) != ERR_OK)
			{
				LWIP_DEBUGF(NETIF_DEBUG, ("ethernetif_input: IP input error\n"));
				pbuf_free(frame);
				frame = NULL;
			}				
      taskEXIT_CRITICAL();
	}
}


err_t ethernetif_init(struct netif *netif)
{	
  	struct ethernetif *ethernetif;
    LWIP_ASSERT("netif != NULL", (netif != NULL));
	
		ethernetif = mem_malloc(sizeof(struct ethernetif));

		if (ethernetif == NULL) {
			PRINT_ERR("ethernetif_init: out of memory\n");
			return ERR_MEM;
		}
  
    netif->mtu = RNDIS_MTU;
    netif->flags = NETIF_FLAG_BROADCAST | NETIF_FLAG_ETHARP | NETIF_FLAG_LINK_UP; //| NETIF_FLAG_UP;
    netif->state = NULL;
    netif->name[0] = 'E';
    netif->name[1] = 'X';
	
	  /* set MAC hardware address length */
		netif->hwaddr_len = ETH_HWADDR_LEN;
  
		/* set MAC hardware address */
		netif->hwaddr[0] =  0x20;
		netif->hwaddr[1] =  0x89;
		netif->hwaddr[2] =  0x84;
		netif->hwaddr[3] =  0x6A;
		netif->hwaddr[4] =  0x96;
		netif->hwaddr[5] =  00;
	  
		netif->output = etharp_output;
    netif->linkoutput = linkoutput_fn;

		ethernetif->ethaddr = (struct eth_addr *) &(netif->hwaddr[0]);
		
	  if(sys_sem_new(&rx_sem , 0) == ERR_OK)
    PRINT_DEBUG("sys_sem_new ok\n");
  
   /* create the task that handles the ETH_MAC */
	 sys_thread_new("ETHIN",
                  ethernetif_input,  /* 任务入口函数 */
                  netif,        	  /* 任务入口函数参数 */
                  NETIF_IN_TASK_STACK_SIZE,/* 任务栈大小 */
                  NETIF_IN_TASK_PRIORITY); /* 任务的优先级 */
    return ERR_OK;
}


void rndis_rxproc(const char *data, int size)
{
	BaseType_t  xHigherPriorityTaskWoken;
  memcpy(received, data, size);
  recvSize = size;

	xSemaphoreGiveFromISR(rx_sem, &xHigherPriorityTaskWoken);  //释放二值信号量
  portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
}

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/

