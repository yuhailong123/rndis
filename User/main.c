/**
  ******************************************************************************
  * @file    main.c
  * @author  野火
  * @version V1.0
  * @date    2013-xx-xx
  * @brief   STM32 USB RNDIS 实验(模拟网卡)
  ******************************************************************************
  * @attention
  *
  * 实验平台:野火 F103-指南者 STM32 开发板 
  * 论坛    :http://www.firebbs.cn
  * 淘宝    :https://fire-stm32.taobao.com
  *
  ******************************************************************************
  */ 
  
#include "stm32f10x.h"
#include "./flash/fatfs_flash_spi.h"
#include "./usart/bsp_usart.h"	
#include "./led/bsp_led.h"  
#include "hw_config.h" 
#include "usb_lib.h"
#include "usb_pwr.h"
/* FreeRTOS头文件 */
#include "FreeRTOS.h"
#include "task.h"
#include "usb_rndis_core.h"

#include "netif.h"
#include "dhserver.h"
#include "dnserver.h"
#include "tcpecho.h"
#include "client.h"
#include "sys_arch.h"
#include "tcpclient.h"
/***********************************************************************
  * @ 函数名  ： BSP_Init
  * @ 功能说明： 板级外设初始化，所有板子上的初始化均可放在这个函数里面
  * @ 参数    ：   
  * @ 返回值  ： 无
  *********************************************************************/
static void BSP_Init(void)
{
	/*
	 * STM32中断优先级分组为4，即4bit都用来表示抢占优先级，范围为：0~15
	 * 优先级分组只需要分组一次即可，以后如果有其他的任务需要用到中断，
	 * 都统一用这个优先级分组，千万不要再分组，切忌。
	 */
	NVIC_PriorityGroupConfig( NVIC_PriorityGroup_4 );
	
	/* LED 初始化 */
	LED_GPIO_Config();

	/* 串口初始化	*/
	USART_Config();
  
		/*设置USB时钟为48M*/
	Set_USBClock();
 	
	/*配置USB中断(包括SDIO中断)*/
	USB_Interrupts_Config();
 
	/*USB初始化*/
 	USB_Init();
 
 	while (bDeviceState != CONFIGURED);	 //等待配置完成
	   
}

/***********************************************************************
  * @ 函数名  ： AppTaskCreate
  * @ 功能说明： 为了方便管理，所有的任务创建函数都放在这个函数里面
  * @ 参数    ： 无  
  * @ 返回值  ： 无
  **********************************************************************/

static void AppTaskCreate(void)
{
	TCPIP_Init();

	while (!netif_is_up(&gnetif)){};

	while (dhserv_init(&dhcp_config) != ERR_OK){};

	while (dnserv_init(&ipaddr, 53, dns_query_proc) != ERR_OK){};	
//		TCP_Client_Init();
//	client_init();
	tcpecho_init();
  while (1)
  {
//
		 vTaskDelay(300);
  }
}

static TaskHandle_t AppTaskCreate_Handle = NULL;/* 创建任务句柄 */
int main(void)
{
  BaseType_t xReturn = pdPASS;/* 定义一个创建信息返回值，默认为pdPASS */
	BSP_Init();

  
  printf("本例程演示TCP回显实验,电脑发送数据到开发板,开发板将数据返回到电脑上\n\n");
  
  printf("网络连接模型如下：\n\t 电脑<--USB数据线-->开发板\n\n");
  
  printf("实验中使用TCP协议传输数据，电脑作为TCP Client ，开发板作为TCP Server\n\n");
  
  printf("本例程的IP地址均在User/arch/sys_arch.h文件中修改\n\n");
    
  printf("本例程参考<<LwIP应用实战开发指南>> 使用 NETCONN 接口编程\n\n");
  
  printf("请将电脑上位机设置为TCP Client.输入开发板的IP地址(如192.168.0.122)\n\n");  
  
  printf("如需修改IP地址与端口号，则修改对应的宏定义:IP_ADDR0,IP_ADDR1,IP_ADDR2,IP_ADDR3,LOCAL_PORT\n\n");  
  
   /* 创建AppTaskCreate任务 */
  xReturn = xTaskCreate((TaskFunction_t )AppTaskCreate,  /* 任务入口函数 */
                        (const char*    )"AppTaskCreate",/* 任务名字 */
                        (uint16_t       )512,  /* 任务栈大小 */
                        (void*          )NULL,/* 任务入口函数参数 */
                        (UBaseType_t    )3, /* 任务的优先级 */
                        (TaskHandle_t*  )&AppTaskCreate_Handle);/* 任务控制块指针 */ 
  /* 启动任务调度 */           
  if(pdPASS == xReturn)
    vTaskStartScheduler();   /* 启动任务，开启调度 */
  else
    return -1;  
  
  while(1);   /* 正常不会执行到这里 */  
}

/* -------------------------------------end of file -------------------------------------------- */
