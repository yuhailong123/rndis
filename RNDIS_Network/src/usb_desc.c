/**
  ******************************************************************************
  * @file    usb_desc.c
  * @author  MCD Application Team
  * @version V3.4.0
  * @date    29-June-2012
  * @brief   Descriptors for RNDIS Network Device
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2012 STMicroelectronics</center></h2>
  *
  * Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://www.st.com/software_license_agreement_liberty_v2
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */


/* Includes ------------------------------------------------------------------*/
#include "usb_desc.h"
#include "usb_def.h"

uint8_t RNDIS_DataBuff[RNDIS_DATA_BUFF_SIZE];

const uint8_t RNDIS_DeviceDescriptor[RNDIS_SIZ_DEVICE_DESC] =
  {
		#if 0
    18,                                 /* bLength = 18 bytes */
    USB_DEVICE_DESCRIPTOR_TYPE,         /* bDescriptorType = DEVICE */
    0x00, 0x02,                         /* bcdUSB          = 1.1 0x10,0x01  2.0 0x00,0x02 */
    0xE0,                               /* bDeviceClass    = Wireless Controller */
    0x00,                               /* bDeviceSubClass = Unused at this time */
    0x00,                               /* bDeviceProtocol = Unused at this time */
    0x40,                               /* bMaxPacketSize0 = EP0 buffer size */
    LOBYTE(USBD_VID), HIBYTE(USBD_VID), /* Vendor ID */
    LOBYTE(USBD_PID), HIBYTE(USBD_PID), /* Product ID */
    0xFF, 0xFF,                         /* bcdDevice */
    0x01,
    0x02,
    0x03,
    1
		#endif
		0x12 ,0x01 ,0x00 ,0x02 ,0x00 ,0x00 ,0x00 ,0x40 ,0x95 ,0x2D ,0x0A ,0x60 ,0x19 ,0x04 ,0x01 ,0x02 ,0x03 ,0x01
  };
	
	


const uint8_t RNDIS_ConfigDescriptor[RNDIS_SIZ_CONFIG_DESC] =
  {
    /* Configuration descriptor */
#if 0
    9,                                 /* bLength         = 9 bytes. */
    USB_CONFIGURATION_DESCRIPTOR_TYPE, /* bDescriptorType = CONFIGURATION */
    RNDIS_SIZ_CONFIG_DESC,                         /* wTotalLength    = sizeof(usbd_cdc_CfgDesc) */
		0x00, 
		0x02,                              /* bNumInterfaces  = 2 (RNDIS spec). */
    0x01,                              /* bConfValue      = 1 */
    0x00,                              /* iConfiguration  = unused. */
    0x40,                              /* bmAttributes    = Self-Powered. */
    0x01,                              /* MaxPower        = x2mA */

    /* IAD descriptor */

    0x08, /* bLength */
    0x0B, /* bDescriptorType */
    0x00, /* bFirstInterface */
    0x02, /* bInterfaceCount */
    0xE0, /* bFunctionClass (Wireless Controller) */
    0x01, /* bFunctionSubClass */
    0x03, /* bFunctionProtocol */
    0x00, /* iFunction */

    /* Interface 0 descriptor */
    
    9,                             /* bLength */
    USB_INTERFACE_DESCRIPTOR_TYPE, /* bDescriptorType = INTERFACE */
    0x00,                          /* bInterfaceNumber */
    0x00,                          /* bAlternateSetting */
    1,                             /* bNumEndpoints */
    0xE0,                          /* bInterfaceClass: Wireless Controller */
    0x01,                          /* bInterfaceSubClass */
    0x03,                          /* bInterfaceProtocol */
    0,                             /* iInterface */

    /* Interface 0 functional descriptor */

    /* Header Functional Descriptor */
    0x05, /* bFunctionLength */
    0x24, /* bDescriptorType = CS Interface */
    0x00, /* bDescriptorSubtype */
    0x10, /* bcdCDC = 1.10 */
    0x01, /* bcdCDC = 1.10 */

    /* Call Management Functional Descriptor */
    0x05, /* bFunctionLength */
    0x24, /* bDescriptorType = CS Interface */
    0x01, /* bDescriptorSubtype = Call Management */
    0x00, /* bmCapabilities */
    0x01, /* bDataInterface */

    /* Abstract Control Management Functional Descriptor */
    0x04, /* bFunctionLength */
    0x24, /* bDescriptorType = CS Interface */
    0x02, /* bDescriptorSubtype = Abstract Control Management */
    0x00, /* bmCapabilities = Device supports the notification Network_Connection */

    /* Union Functional Descriptor */
    0x05, /* bFunctionLength */
    0x24, /* bDescriptorType = CS Interface */
    0x06, /* bDescriptorSubtype = Union */
    0x00, /* bControlInterface = "RNDIS Communications Control" */
    0x01, /* bSubordinateInterface0 = "RNDIS Ethernet Data" */

    /* Endpoint descriptors for Communication Class Interface */

    7,                            /* bLength         = 7 bytes */
    USB_ENDPOINT_DESCRIPTOR_TYPE, /* bDescriptorType = ENDPOINT */
    RNDIS_NOTIFICATION_IN_EP,     /* bEndpointAddr   = IN - EP3 */
    0x03,                         /* bmAttributes    = Interrupt endpoint */
    8, 0,                         /* wMaxPacketSize */
    0x01,                         /* bInterval       = 1 ms polling from host */

    /* Interface 1 descriptor */

    9,                             /* bLength */
    USB_INTERFACE_DESCRIPTOR_TYPE, /* bDescriptorType */
    0x01,                          /* bInterfaceNumber */
    0x00,                          /* bAlternateSetting */
    2,                             /* bNumEndpoints */
    0x0A,                          /* bInterfaceClass: CDC */
    0x00,                          /* bInterfaceSubClass */
    0x00,                          /* bInterfaceProtocol */
    0x00,                          /* uint8  iInterface */

    /* Endpoint descriptors for Data Class Interface */

    7,                            /* bLength         = 7 bytes */
    USB_ENDPOINT_DESCRIPTOR_TYPE, /* bDescriptorType = ENDPOINT [IN] */
    RNDIS_DATA_IN_EP,             /* bEndpointAddr   = IN EP */
    0x02,                         /* bmAttributes    = BULK */
    RNDIS_DATA_IN_SZ, 0,          /* wMaxPacketSize */
    0,                            /* bInterval       = ignored for BULK */

    7,                            /* bLength         = 7 bytes */
    USB_ENDPOINT_DESCRIPTOR_TYPE, /* bDescriptorType = ENDPOINT [OUT] */
    RNDIS_DATA_OUT_EP,            /* bEndpointAddr   = OUT EP */
    0x02,                         /* bmAttributes    = BULK */
    RNDIS_DATA_OUT_SZ, 0,         /* wMaxPacketSize */
    0                             /* bInterval       = ignored for BULK */
		#endif
		0x09 ,0x02 ,0x4B ,0x00 ,0x02 ,0x01 ,0x04 ,0x80 ,0xFA ,
		0x08 ,0x0B ,0x00 ,0x02 ,0xE0 ,0x01 ,0x03 ,0x07 ,
		0x09 ,0x04 ,0x00 ,0x00 ,0x01 ,0xE0 ,0x01 ,0x03 ,0x05 ,
		0x05 ,0x24 ,0x00 ,0x10 ,0x01 ,
		0x05 ,0x24 ,0x01 ,0x00 ,0x01 ,
		0x04 ,0x24 ,0x02 ,0x00 ,
		0x05 ,0x24 ,0x06 ,0x00 ,0x01 ,
		0x07 ,0x05 ,0x81 ,0x03 ,0x08 ,0x00 ,0x09 ,
		0x09 ,0x04 ,0x01 ,0x00 ,0x02 ,0x0A ,0x00 ,0x00 ,0x06 ,
		0x07 ,0x05 ,0x83 ,0x02 ,0x40 ,0x00 ,0x00 ,
		0x07 ,0x05 ,0x02 ,0x02 ,0x40 ,0x00 ,0x00
  };
	
	
const uint8_t RNDIS_StringLangID[RNDIS_SIZ_STRING_LANGID] =
  {
    RNDIS_SIZ_STRING_LANGID,
    0x03,
    0x09,
    0x04
  };      /* LangID = 0x0409: U.S. English */

const uint8_t RNDIS_StringVendor[RNDIS_SIZ_STRING_VENDOR] =
  {
    RNDIS_SIZ_STRING_VENDOR, /* Size of manufacturer string */
    0x03,           /* bDescriptorType = String descriptor */
    /* Manufacturer: "STMicroelectronics" */
    'S', 0, 'T', 0, 'M', 0, 'i', 0, 'c', 0, 'r', 0, 'o', 0, 'e', 0,
    'l', 0, 'e', 0, 'c', 0, 't', 0, 'r', 0, 'o', 0, 'n', 0, 'i', 0,
    'c', 0, 's', 0
  };
const uint8_t RNDIS_StringProduct[RNDIS_SIZ_STRING_PRODUCT] =
  {
    RNDIS_SIZ_STRING_PRODUCT,
    0x03,
    /* Product name: "STM32F10x:USB RNDIS Network" */
    'S', 0, 'T', 0, 'M', 0, '3', 0, '2', 0, ' ', 0, 'M', 0, 'a', 0, 's', 0,
    's', 0, ' ', 0, 'S', 0, 't', 0, 'o', 0, 'r', 0, 'a', 0, 'g', 0, 'e', 0

  };

uint8_t RNDIS_StringSerial[RNDIS_SIZ_STRING_SERIAL] =
  {
    RNDIS_SIZ_STRING_SERIAL,
    0x03,
    /* Serial number*/
#if defined(STM32L1XX_MD) || defined(STM32L1XX_HD)|| defined(STM32L1XX_MD_PLUS)
    'S', 0, 'T', 0, 'M', 0, '3', 0, '2', 0, 'L', 0, '1', 0
#else
    'S', 0, 'T', 0, 'M', 0, '3', 0, '2', 0, '1', 0, '0', 0      
#endif /* STM32L1XX_XD */
  };
const uint8_t RNDIS_StringInterface[RNDIS_SIZ_STRING_INTERFACE] =
  {
    RNDIS_SIZ_STRING_INTERFACE,
    0x03,
    /* Interface 0: "ST RNDIS" */
    'S', 0, 'T', 0, ' ', 0, 'M', 0, 'a', 0, 's', 0, 's', 0
  };

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
