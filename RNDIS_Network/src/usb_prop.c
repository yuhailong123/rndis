/**
  ******************************************************************************
  * @file    usb_prop.c
  * @author  MCD Application Team
  * @version V3.4.0
  * @date    29-June-2012
  * @brief   All processing related to RNDIS Demo
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2012 STMicroelectronics</center></h2>
  *
  * Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://www.st.com/software_license_agreement_liberty_v2
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */


/* Includes ------------------------------------------------------------------*/
#include "hw_config.h"
#include "usb_lib.h"
#include "usb_desc.h"
#include "usb_pwr.h"
#include "usb_prop.h"
#include "usb_core.h"
#include "rndis_protocol.h"
/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
 uint32_t Max_Lun = 0; 
//#endif

DEVICE Device_Table =
  {
    EP_NUM,
    1
  };

DEVICE_PROP Device_Property =
  {
    RNDIS_init,
    RNDIS_Reset,
    RNDIS_Status_In,
    RNDIS_Status_Out,
    RNDIS_Data_Setup,
    RNDIS_NoData_Setup,
    RNDIS_Get_Interface_Setting,
    RNDIS_GetDeviceDescriptor,
    RNDIS_GetConfigDescriptor,
    RNDIS_GetStringDescriptor,
    0,
   	0x40 /*MAX PACKET SIZE*/
  };

USER_STANDARD_REQUESTS User_Standard_Requests =
  {
    RNDIS_Network_GetConfiguration,
    RNDIS_Network_SetConfiguration,
    RNDIS_Network_GetInterface,
    RNDIS_Network_SetInterface,
    RNDIS_Network_GetStatus,
    RNDIS_Network_ClearFeature,
    RNDIS_Network_SetEndPointFeature,
    RNDIS_Network_SetDeviceFeature,
    RNDIS_Network_SetDeviceAddress
  };

ONE_DESCRIPTOR Device_Descriptor =
  {
    (uint8_t*)RNDIS_DeviceDescriptor,
    RNDIS_SIZ_DEVICE_DESC
  };

ONE_DESCRIPTOR Config_Descriptor =
  {
    (uint8_t*)RNDIS_ConfigDescriptor,
    RNDIS_SIZ_CONFIG_DESC
  };

ONE_DESCRIPTOR String_Descriptor[5] =
  {
    {(uint8_t*)RNDIS_StringLangID, RNDIS_SIZ_STRING_LANGID},
    {(uint8_t*)RNDIS_StringVendor, RNDIS_SIZ_STRING_VENDOR},
    {(uint8_t*)RNDIS_StringProduct, RNDIS_SIZ_STRING_PRODUCT},
    {(uint8_t*)RNDIS_StringSerial, RNDIS_SIZ_STRING_SERIAL},
    {(uint8_t*)RNDIS_StringInterface, RNDIS_SIZ_STRING_INTERFACE},
  };

	

ONE_DESCRIPTOR RNDIS_Out =
{
	(uint8_t*)RNDIS_DataBuff,
	RNDIS_DATA_BUFF_SIZE
};


/* Private function prototypes -----------------------------------------------*/
/* Extern function prototypes ------------------------------------------------*/
/* Private functions ---------------------------------------------------------*/
/*******************************************************************************
* Function Name  : RNDIS_init
* Description    : RNDIS Network init routine.
* Input          : None.
* Output         : None.
* Return         : None.
*******************************************************************************/
void RNDIS_init()
{
  /* Update the serial number string descriptor with the data from the unique
  ID*/
  Get_SerialNum();

  pInformation->Current_Configuration = 0;

  /* Connect the device */
  PowerOn();

  /* Perform basic device initialization operations */
  USB_SIL_Init();

  bDeviceState = UNCONNECTED;
}

/*******************************************************************************
* Function Name  : RNDIS_Reset
* Description    : RNDIS Network reset routine.
* Input          : None.
* Output         : None.
* Return         : None.
*******************************************************************************/
void RNDIS_Reset()
{
  /* Set the device as not configured */
  Device_Info.Current_Configuration = 0;

  /* Current Feature initialization */
  pInformation->Current_Feature = RNDIS_ConfigDescriptor[7];

#ifdef STM32F10X_CL   
  
  /* EP0 is already configured by USB_SIL_Init() function */

  /* Init EP1 IN as Bulk endpoint */
  OTG_DEV_EP_Init(EP1_IN, OTG_DEV_EP_TYPE_BULK, BULK_MAX_PACKET_SIZE);
  
  /* Init EP2 OUT as Bulk endpoint */
  OTG_DEV_EP_Init(EP2_OUT, OTG_DEV_EP_TYPE_BULK, BULK_MAX_PACKET_SIZE); 
  
#else 

  SetBTABLE(BTABLE_ADDRESS);

  /* Initialize Endpoint 0 */
  SetEPType(ENDP0, EP_CONTROL);
  SetEPTxStatus(ENDP0, EP_TX_NAK);
  SetEPRxAddr(ENDP0, ENDP0_RXADDR);
  SetEPRxCount(ENDP0, Device_Property.MaxPacketSize);
  SetEPTxAddr(ENDP0, ENDP0_TXADDR);
  Clear_Status_Out(ENDP0);
  SetEPRxValid(ENDP0);

  /* Initialize Endpoint 1 */
  SetEPType(ENDP1, EP_INTERRUPT);
  SetEPTxAddr(ENDP1, ENDP1_TXADDR);
  SetEPTxStatus(ENDP1, EP_TX_VALID);
  SetEPRxStatus(ENDP1, EP_RX_DIS);

  /* Initialize Endpoint 2 */
  SetEPType(ENDP3, EP_BULK);
  SetEPTxCount(ENDP3, Device_Property.MaxPacketSize);
  SetEPRxStatus(ENDP3, EP_RX_DIS);
  SetEPTxStatus(ENDP3, EP_TX_NAK);

  /* Initialize Endpoint 2 */
  SetEPType(ENDP2, EP_BULK);
  SetEPRxAddr(ENDP2, ENDP2_RXADDR);
  SetEPRxCount(ENDP2, Device_Property.MaxPacketSize);
  SetEPTxStatus(ENDP2, EP_TX_DIS);
  SetEPRxStatus(ENDP2, EP_RX_VALID);
  /****************/




  SetEPRxCount(ENDP0, Device_Property.MaxPacketSize);
  SetEPRxValid(ENDP0);

  /* Set the device to response on default address */
  SetDeviceAddress(0);
#endif /* STM32F10X_CL */

  bDeviceState = ATTACHED;

}

/*******************************************************************************
* Function Name  : RNDIS_Network_SetConfiguration
* Description    : Handle the SetConfiguration request.
* Input          : None.
* Output         : None.
* Return         : None.
*******************************************************************************/
void RNDIS_Network_SetConfiguration(void)
{
  if (pInformation->Current_Configuration != 0)
  {
    /* Device configured */
    bDeviceState = CONFIGURED;

#ifdef STM32F10X_CL 
    /* Init EP1 IN as Bulk endpoint */
    OTG_DEV_EP_Init(EP1_IN, OTG_DEV_EP_TYPE_BULK, BULK_MAX_PACKET_SIZE);
  
    /* Init EP2 OUT as Bulk endpoint */
    OTG_DEV_EP_Init(EP2_OUT, OTG_DEV_EP_TYPE_BULK, BULK_MAX_PACKET_SIZE);     
#else    
    ClearDTOG_TX(ENDP1);
    ClearDTOG_TX(ENDP2);
    ClearDTOG_RX(ENDP3);
#endif /* STM32F10X_CL */

  }
}


/*******************************************************************************
* Function Name  : RNDIS_Network_SetConfiguration.
* Description    : Update the device state to addressed.
* Input          : None.
* Output         : None.
* Return         : None.
*******************************************************************************/
void RNDIS_Network_SetDeviceAddress (void)
{
  bDeviceState = ADDRESSED;
}
/*******************************************************************************
* Function Name  : RNDIS_Network_In
* Description    : RNDIS Network Status IN routine.
* Input          : None.
* Output         : None.
* Return         : None.
*******************************************************************************/
void RNDIS_Status_In(void)
{
  return;
}

/*******************************************************************************
* Function Name  : RNDIS_Network_Out
* Description    : RNDIS Network Status OUT routine.
* Input          : None.
* Output         : None.
* Return         : None.
*******************************************************************************/
void RNDIS_Status_Out(void)
{
  return;
}

uint8_t *RNDIS_Network_Out(uint16_t Length)
{
	  return Standard_GetDescriptorData(Length, &RNDIS_Out );
}

/*******************************************************************************
* Function Name  : RNDIS_Data_Setup.
* Description    : Handle the data class specific requests..
* Input          : RequestNo.
* Output         : None.
* Return         : RESULT.
*******************************************************************************/
RESULT RNDIS_Data_Setup(uint8_t RequestNo)
{
  uint8_t *(*CopyRoutine)(uint16_t);
	CopyRoutine = NULL;
	if (pInformation->USBbmRequestType & 0x80)
	{
		if(pInformation->USBbRequest == 0x01)
		{
			if (Type_Recipient == (CLASS_REQUEST | INTERFACE_RECIPIENT))
			{	
					uint32_t *Buff;
					pInformation->Ctrl_Info.Usb_wOffset = 0;
					Buff = (uint32_t*) RNDIS_Network_Out(1);
					pInformation->Ctrl_Info.Usb_wLength =  Buff[1];
					CopyRoutine = RNDIS_Network_Out;
					pInformation->Ctrl_Info.CopyData = CopyRoutine;
			}			
		}			
	}
	else
	{
		if(pInformation->USBbRequest == 0)
		{
			if (Type_Recipient == (CLASS_REQUEST | INTERFACE_RECIPIENT))
			{	

					pInformation->Ctrl_Info.Usb_wOffset = 0;
					pInformation->Ctrl_Info.Usb_wLength = pInformation->USBwLength;
					CopyRoutine = RNDIS_Network_Out;
					pInformation->Ctrl_Info.CopyData = CopyRoutine;
			}		
		}

	}
	return USB_SUCCESS;
	
}

/*******************************************************************************
* Function Name  : RNDIS_NoData_Setup.
* Description    : Handle the no data class specific requests.
* Input          : RequestNo.
* Output         : None.
* Return         : RESULT.
*******************************************************************************/
RESULT RNDIS_NoData_Setup(uint8_t RequestNo)
{
  if ((Type_Recipient == (CLASS_REQUEST | INTERFACE_RECIPIENT))
      && (RequestNo == RNDIS_Network_RESET) && (pInformation->USBwValue == 0)
      && (pInformation->USBwIndex == 0) && (pInformation->USBwLength == 0x00))
  {
   #ifdef STM32F10X_CL 
    /* Init EP1 IN as Bulk endpoint */
    OTG_DEV_EP_Init(EP1_IN, OTG_DEV_EP_TYPE_BULK, BULK_MAX_PACKET_SIZE);
  
    /* Init EP2 OUT as Bulk endpoint */
    OTG_DEV_EP_Init(EP2_OUT, OTG_DEV_EP_TYPE_BULK, BULK_MAX_PACKET_SIZE);     
   #else
    /* Initialize Endpoint 1 */
    ClearDTOG_TX(ENDP1);

    /* Initialize Endpoint 2 */
    ClearDTOG_TX(ENDP2);
		ClearDTOG_RX(ENDP3);
   #endif /* STM32F10X_CL */


    return USB_SUCCESS;
  }
  return USB_UNSUPPORT;
}

/*******************************************************************************
* Function Name  : RNDIS_Get_Interface_Setting
* Description    : Test the interface and the alternate setting according to the
*                  supported one.
* Input          : uint8_t Interface, uint8_t AlternateSetting.
* Output         : None.
* Return         : RESULT.
*******************************************************************************/
RESULT RNDIS_Get_Interface_Setting(uint8_t Interface, uint8_t AlternateSetting)
{
  if (AlternateSetting > 0)
  {
    return USB_UNSUPPORT;/* in this application we don't have AlternateSetting*/
  }
  else if (Interface > 0)
  {
    return USB_UNSUPPORT;/*in this application we have only 1 interfaces*/
  }
  return USB_SUCCESS;
}

/*******************************************************************************
* Function Name  : RNDIS_GetDeviceDescriptor
* Description    : Get the device descriptor.
* Input          : uint16_t Length.
* Output         : None.
* Return         : None.
*******************************************************************************/
uint8_t *RNDIS_GetDeviceDescriptor(uint16_t Length)
{
  return Standard_GetDescriptorData(Length, &Device_Descriptor );
}

/*******************************************************************************
* Function Name  : RNDIS_GetConfigDescriptor
* Description    : Get the configuration descriptor.
* Input          : uint16_t Length.
* Output         : None.
* Return         : None.
*******************************************************************************/
uint8_t *RNDIS_GetConfigDescriptor(uint16_t Length)
{
  return Standard_GetDescriptorData(Length, &Config_Descriptor );
}

/*******************************************************************************
* Function Name  : RNDIS_GetStringDescriptor
* Description    : Get the string descriptors according to the needed index.
* Input          : uint16_t Length.
* Output         : None.
* Return         : None.
*******************************************************************************/
uint8_t *RNDIS_GetStringDescriptor(uint16_t Length)
{
  uint8_t wValue0 = pInformation->USBwValue0;

  if (wValue0 > 5)
  {
    return NULL;
  }
  else
  {
    return Standard_GetDescriptorData(Length, &String_Descriptor[wValue0]);
  }
}


/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
