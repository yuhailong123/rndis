/**
  ******************************************************************************
  * @file    usb_prop.h
  * @author  MCD Application Team
  * @version V3.4.0
  * @date    29-June-2012
  * @brief   All processing related to RNDIS Network Demo (Endpoint 0)
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2012 STMicroelectronics</center></h2>
  *
  * Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://www.st.com/software_license_agreement_liberty_v2
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */


/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __usb_prop_H
#define __usb_prop_H
/* Includes ------------------------------------------------------------------*/
/* Exported types ------------------------------------------------------------*/

/* Exported constants --------------------------------------------------------*/
#define RNDIS_Network_GetConfiguration          NOP_Process
/* #define RNDIS_Network_SetConfiguration          NOP_Process*/
#define RNDIS_Network_GetInterface              NOP_Process
#define RNDIS_Network_SetInterface              NOP_Process
#define RNDIS_Network_GetStatus                 NOP_Process
#define RNDIS_Network_ClearFeature              NOP_Process
#define RNDIS_Network_SetEndPointFeature        NOP_Process
#define RNDIS_Network_SetDeviceFeature          NOP_Process
/*#define RNDIS_Network_SetDeviceAddress          NOP_Process*/

/* RNDIS Network Requests*/
#define RNDIS_Network_RESET         0xFF
#define LUN_DATA_LENGTH            1

/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */
void RNDIS_init(void);
void RNDIS_Reset(void);
void RNDIS_Network_SetConfiguration(void);
void RNDIS_Network_SetDeviceAddress (void);
void RNDIS_Status_In (void);
void RNDIS_Status_Out (void);
RESULT RNDIS_Data_Setup(uint8_t);
RESULT RNDIS_NoData_Setup(uint8_t);
RESULT RNDIS_Get_Interface_Setting(uint8_t Interface, uint8_t AlternateSetting);
uint8_t *RNDIS_GetDeviceDescriptor(uint16_t );
uint8_t *RNDIS_GetConfigDescriptor(uint16_t);
uint8_t *RNDIS_GetStringDescriptor(uint16_t);

#endif /* __usb_prop_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/

