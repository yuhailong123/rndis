/**
  ******************************************************************************
  * @file    usb_desc.h
  * @author  MCD Application Team
  * @version V3.4.0
  * @date    29-June-2012
  * @brief   Descriptor Header for RNDIS Network Device
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2012 STMicroelectronics</center></h2>
  *
  * Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://www.st.com/software_license_agreement_liberty_v2
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */


/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __USB_DESC_H
#define __USB_DESC_H

/* Includes ------------------------------------------------------------------*/
#if defined(STM32L1XX_MD) || defined(STM32L1XX_HD)|| defined(STM32L1XX_MD_PLUS)
 #include "stm32l1xx.h"
#else
 #include "stm32f10x.h"
#endif /* STM32L1XX_XD */
 
/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported define -----------------------------------------------------------*/
#define RNDIS_SIZ_DEVICE_DESC              18
#define RNDIS_SIZ_CONFIG_DESC              75

#define RNDIS_SIZ_STRING_LANGID            4
#define RNDIS_SIZ_STRING_VENDOR            38
#define RNDIS_SIZ_STRING_PRODUCT           38
#define RNDIS_SIZ_STRING_SERIAL            26
#define RNDIS_SIZ_STRING_INTERFACE         16
#define RNDIS_DATA_BUFF_SIZE               128
#define USBD_VID                           0x4E44
#define USBD_PID                           0x4953
#define USBD_LANGID_STRING                 0x409

#define RNDIS_NOTIFICATION_IN_SZ                0x08
#define RNDIS_DATA_IN_SZ                        0x40
#define RNDIS_DATA_OUT_SZ                       0x40

#define USB_DEVICE_DESCRIPTOR_TYPE              0x01
#define RNDIS_NOTIFICATION_IN_EP                0x81
#define RNDIS_DATA_IN_EP                        0x83
#define RNDIS_DATA_OUT_EP                       0x02



#define USB_CONFIGURATION_DESCRIPTOR_TYPE       0x02
#define USB_INTERFACE_DESCRIPTOR_TYPE           0x04
#define USB_ENDPOINT_DESCRIPTOR_TYPE            0x05
/* Exported functions ------------------------------------------------------- */
extern const uint8_t RNDIS_DeviceDescriptor[RNDIS_SIZ_DEVICE_DESC];
extern const uint8_t RNDIS_ConfigDescriptor[RNDIS_SIZ_CONFIG_DESC];

extern const uint8_t RNDIS_StringLangID[RNDIS_SIZ_STRING_LANGID];
extern const uint8_t RNDIS_StringVendor[RNDIS_SIZ_STRING_VENDOR];
extern const uint8_t RNDIS_StringProduct[RNDIS_SIZ_STRING_PRODUCT];
extern uint8_t RNDIS_StringSerial[RNDIS_SIZ_STRING_SERIAL];
extern const uint8_t RNDIS_StringInterface[RNDIS_SIZ_STRING_INTERFACE];
extern uint8_t RNDIS_DataBuff[RNDIS_DATA_BUFF_SIZE];
#endif /* __USB_DESC_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/


