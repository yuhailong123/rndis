# STM32F103ZET6 USB虚拟网卡实验

#### 介绍
STM32F103ZET6 USB虚拟网卡实验基于野火霸道开发板

#### 软件架构
电脑<---->USB线<------>USB协议<------>  | RNDIS协议|  LwIP协议栈


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1. 使用串口或者是仿真器下载好程序

2. 电脑连接开发板的USB转串口，打开串口调试助手设置波特率为115200 
![输入图片说明](https://images.gitee.com/uploads/images/2021/0422/171338_a5ba5174_7643292.png "企业微信截图_16190820557191.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0422/171431_d96417ba_7643292.png "企业微信截图_1619081709440.png")
3. 连接板子的USB Device接口，等待电脑配置完成之后，设置网络调试助手为TCP Client模式，服务器的地址改为192.168.0.122，端口号为5001
![输入图片说明](https://images.gitee.com/uploads/images/2021/0422/171520_1b2dbe3e_7643292.png "企业微信截图_16190821101156.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0422/171552_a3a16673_7643292.png "企业微信截图_16190815937307.png")
4. 点击连接，发送数据即可看到板子回显的数据

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
